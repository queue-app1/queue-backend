const express = require('express');
const connectDb = require("./config/db");
const {  user, event, queue, participant } = require("./Routes/index");
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const socketIO = require("socket.io");
const http = require("http");

const app = express();
connectDb();
const cors = require("cors");



const port = process.env.PORT || 5000;
const sport= process.env.PORT || 4000;

app.use(cors());
app.use(express.json());

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Events REST API',
            description: "A REST API built with Express and MongoDB. This API provides events ."
        },
    },
    apis: [ "./Routes/user.js", "./Routes/event.js", "./Routes/queue.js", "./Routes/userEvent.js"]
}


app.use('/user', user)
app.use('/event', event)
app.use('/queue', queue)
app.use('/participant', participant)



const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.get('/', (req, res) => {
    res.send('Hello Wor!')
});

let server = app.listen(port, () => {
    console.log(`server is running on port': ${port}`)
});
let io = socketIO(server, {
    cors: {
        origin: "https://sample-events-project-frontend.herokuapp.com",
        methods: ["GET", "POST"]
    }
});

io.on("connection", (socket) => {
    // console.log("new user connected");
    socket.on("eventAdded", (user) => {
        io.emit("eventAdded", user);
        console.log("some added event");
    })

    // when admin addes new event
    socket.on("eventHasBeenAdded", () => {
        io.emit("eventListUpdated");
        console.log("some event has been added");
    })

    // when admin leaves the meeting
    socket.on("leaveAdminMeeting", () => {
        io.emit("adminLeftMeeting");
        console.log("admin left meeting");
    })

    // when user leaves the event
    socket.on("leaveUserMeeting", () => {
        io.emit("userLeftMeeting");
        console.log("user left meeting");
    })
    // when user raises or lowers his/her hand
    socket.on("userRaisesHand", () => {
        io.emit("queueUpdated");
        console.log("user left meeting");
    })
    socket.on("userLowersHand", () => {
        io.emit("queueUpdated");
    })
    // when user joins the meeting
    socket.on("newUserJoined", () => {
        io.emit("participantsUpdated");
    })

    // when adming toggles the queue
    socket.on("queueStatusChanged", () => {
        io.emit("eventQueueUpdated");
    })

    socket.on('disconnect', function (socket) {
        counter = counter > 0 ? counter - 1 : counter;
        io.emit('totalUser', counter)
    });
});


let counter = 0;
