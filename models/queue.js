const mongoose = require("mongoose");
const { stringify } = require("nodemon/lib/utils");

const queueSchema = new mongoose.Schema({
    displayName: {type: String},
    event: {type: mongoose.Schema.Types.ObjectId, ref: 'event'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'},
    active: {type: Boolean, default: true}
    
});

module.exports = mongoose.model("queue", queueSchema)