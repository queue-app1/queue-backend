const mongoose = require("mongoose");
const { stringify } = require("nodemon/lib/utils");

const userSchema = new mongoose.Schema({
    username: {type: String, unique: true},
    password: {type: String},
    role: { type: String, default: "User"}
    // toke: { type: String}
});

module.exports = mongoose.model("user", userSchema)