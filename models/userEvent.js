const mongoose = require("mongoose");
const { stringify } = require("nodemon/lib/utils");

const userEventSchema = new mongoose.Schema({
    displayName: {type: String},
    event: {type: mongoose.Schema.Types.ObjectId, ref: 'event'},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'user'}
     
});

module.exports = mongoose.model("userEvent", userEventSchema)