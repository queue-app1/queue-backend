const mongoose = require("mongoose");
const { stringify } = require("nodemon/lib/utils");

const eventSchema = new mongoose.Schema({
    title: {type: String, unique: true},
    startDate: {type: Date},
    endDate: { type: Date,},
    has_ended: {type: Boolean, default: false},
    queueOn: {type: Boolean, default: true}
    // toke: { type: String}
});

module.exports = mongoose.model("event", eventSchema)