
const UserEvent = require('../models/userEvent');


async function getAll()
{
    let userEvents=[];
    try{
       userEvents= await UserEvent.find({});

    }
    catch(err)
    {
        return { success: false, message: err.message}
    }

    return {
        success: true,
        data: userEvents
    }
}

async function getById(id) {
    let userEvent;
    try {
        userEvent = await UserEvent.findById(id);
        if (userEvent == null) {
            return { success: false, message: 'Cannot find user-event' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: userEvent,
    };
}

async function getByEventId(event) {
    let participants=[];
    try {
        participants = await UserEvent.find({event: event});
        if (participants == null) {
            return { success: false, message: 'Cannot find Event' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: participants,
    };
}

async function getByUserId(user) {
    let events=[];
    try {
        events = await UserEvent.findAll({user:user});
        if (events == null) {
            return { success: false, message: 'Cannot find userEvent' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: events,
    };
}

async function add(body) {
    const participant = new UserEvent(body);

    try {
        const newParticipant = await participant.save();
        return {
            success: true,
            data: newParticipant,
        };
    } catch (err) {
        console.log(err)
        return { success: false, message: "Failed to add user event" };
    }
}


module.exports = {
    getById,
    add,
    getAll,
    getByEventId,
    getByUserId
}