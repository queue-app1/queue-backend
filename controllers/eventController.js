const Event = require('../models/event');
const UserEvent = require('../models/userEvent');
let {  getByEventId} = require('../controllers/userEventController')
let {  getQueueByEventId } = require('../controllers/queueController');
const queue = require('../models/queue');


async function getAll()
{
    let events=[];
    try{
       events= await Event.find({});

    }
    catch(err)
    {
        return { success: false, message: err.message}
    }

    return {
        success: true,
        data: events
    }
}

async function getEventSummaryById(id) {
    console.log("under get event summary by id");
    let event;
    let eventParticipants = 0;
    let queues = 0;
    try {
        event = await Event.findById(id);
        var participants = await getByEventId(id);
        var eventQueues = await getQueueByEventId(id);
        if (event == null) {
            return { success: false, message: 'Cannot find event' };
        }
        if(participants == null)
        {
            eventParticipants = participants?.length;
        }
        eventParticipants = participants.data?.length;

        queues = eventQueues?.data.length;

        console.log(queues);
        console.log(eventParticipants );

        event['participants'] = eventParticipants;
        if(eventQueues == null)
        {
            event['queues'] = 0
        }
        else{
            event['queues'] = queues;
        }

        console.log(event);
        // event['queues'] = queues;
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: {
            title: event.title,
            startDate: event.startDate,
            has_ended: event.has_ended,
            queueOn: event.queueOn,
            participants: eventParticipants,
            queues: queues
        },
    };
}

async function getEventById(id) {
    let event;
    try {
        event = await Event.findById(id);
        if (event == null) {
            return { success: false, message: 'Cannot find event' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: event,
    };
}

async function addEvent(body) {
    const event = new Event(body);

    try {
        const newEvent = await event.save();
        return {
            success: true,
            data: newEvent,
        };
    } catch (err) {
        return { success: false, message: "Failed to add event" };
    }
}

async function updateEvent(id,body) {
    let event;
    try {
        event = await Event.findById(id);
        if (event== null) {
            return { success: false, message: 'Cannot find event' };
        }
        
        if (body.title != null) {
            event.title = body.title
        }
        if (body.startDate != null) {
            event.startDate = body.startDate
        }
        if (body.endDate != null) {
            event.endDate = body.endDate
        }
        event.has_ended = body.has_ended;
        event.queueOn = body.queueOn;


        try {
            const updatedEvent = await event.save()
            return {
                success: true,
                data: updatedEvent,
                message: "Event updated successfully"
            };
        } catch (err) {
            return { sucess: false ,message: "Failed to update event" };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }
}



module.exports = {
    getEventById,
    getEventSummaryById,
    addEvent,
    updateEvent,
    getAll
}