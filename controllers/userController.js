const bcrypt = require("bcryptjs/dist/bcrypt");
const res = require("express/lib/response");
const User = require("../models/user");
const jwt = require("jsonwebtoken");

async function getAll()
{
    let users=[];
    try{
       users= await User.find({});

    }
    catch(err)
    {
        return { success: false, message: err.message}
    }

    return {
        success: true,
        data: users
    }
}

async function register(userInfo)
{
  try{
      const { username, password} = userInfo;

    //   validate user input
    if(!username && password)
    {
        return  { success: false, meesage: "All input required"}
    }
    // check if user already exisists
    // validate if user exist in our database
    const oldUser= await User.findOne({username});

    if(oldUser)
    {
        return { success: false, message : "User Already Exist"}
    }

    // Encrypt user password
    encryptedPassword = await bcrypt.hash(password, 10);

    console.log(encryptedPassword);

    // Create user in our database
    const user = await User.create({
        username: username.toLowerCase(), // sanitize: convert email to lowercase
        password: encryptedPassword,
      });
  
      // Create token
      const token = jwt.sign(
        { user_id: user._id, username },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );
      // save user token
      user.token = token;
     
  
      // return new user
    //   res.status(201).json(user);
    return { success: true, data: token}

  }
  catch (err) {
    console.log(err);
  }

}

async function login(body)
{
  // Our login logic starts here
  try {
    // Get user input
    const { username, password } = body;

    // Validate user input
    if (!(username && password)) {
      // res.status(400).send("All input is required");
      return { success: false, message : "All fields required"}
    }
    // Validate if user exist in our database
    const user = await User.findOne({ username });

    if (user && (await bcrypt.compare(password, user.password))) {
      // Create token
      const token = jwt.sign(
        { user_id: user._id, username },
        process.env.TOKEN_KEY,
        {
          expiresIn: "2h",
        }
      );

      // save user token
      user['token'] = token;

      var userData={
        username: user.username,
        password: user.password,
        role: user.role,
        token: token,
        _id: user._id
      }

      // user
      // res.status(200).json(user);
      return { success: true, data: userData}
    }
    // res.status(400).send("Invalid Credentials");
    return { success: false, message : "Login Failed"}
  } catch (err) {
    console.log(err);
  }

}

module.exports = {
    register,
    login,
    getAll
}
