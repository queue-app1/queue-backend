
const Queue = require('../models/queue');


async function getAll()
{
    let queues=[];
    try{
       queues= await Queue.find({});

    }
    catch(err)
    {
        return { success: false, message: err.message}
    }

    return {
        success: true,
        data: queues
    }
}

async function getAllActiveEventQueue(id)
{
    let queues=[];
    try{
       queues= await Queue.find({event: id, active: true});

    }
    catch(err)
    {
        return { success: false, message: err.message}
    }

    return {
        success: true,
        data: queues
    }
}


async function getQueueById(id) {
    let queue;
    try {
        queue = await Queue.findById(id);
        if (queue == null) {
            return { success: false, message: 'Cannot find queue' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: queue,
    };
}

async function getQueueByEventId(event) {
    let queue=[];
    try {
        queue = await Queue.find({event: event});
        if (queue == null) {
            return { success: false, message: 'Cannot find queue' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: queue,
    };
}

async function checkIfUserRaisedHand(user, event) {
    let queue;
    try {
        queue = await Queue.find({event: event, user: user});
        if (queue == null) {
            return { success: false, data: false };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: queue,
    };
}

async function getEnabledQueueByEventId(event) {
    let queues=[];
    try {
        queue = await Queue.findAll({event:event, disabled: false});
        if (queue == null) {
            return { success: false, message: 'Cannot find queue' };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }

    return {
        success: true,
        data: queues,
    };
}

async function addQueue(body) {
    console.log("under add queue");
    console.log(body);
    const queue = new Queue(body);

    try {
        const newQueue = await queue.save();
        return {
            success: true,
            data: newQueue,
        };
    } catch (err) {
        // return err
        return { success: false, message: err };
    }
}

async function deactivateQueueByEventId(id)
{
    try{
        const response= await Queue.updateMany({event: id}, {active: false});
         return {success: true}
    }
    catch(err)
    {
        // return err
        return { success: false, message: err };
    }

}

async function deactivateQueueById(id)
{
    try{
        const response= await Queue.updateMany({_id: id}, {active: false});
         return {success: true}
    }
    catch(err)
    {
        // return err
        return { success: false, message: err };
    }

}

async function deactivateQueueByUserId(userId, eventId)
{
    try{
        const response= await Queue.updateMany({event: eventId, user: userId}, {active: false});
         return {success: true}
    }
    catch(err)
    {
        // return err
        return { success: false, message: err };
    }
}

async function updateQueue(id, disabled = null) {
    let queue;
    try {
        queue = await Queue.findById(id);
        if (queue == null) {
            return { success: false, message: 'Cannot find queue' };
        }
        if (disabled != null) {
            queue.title = title
        }

        try {
            const updatedQueue = await queue.save()
            return {
                success: true,
                data: updatedQueue,
                message: "Queue updated successfully"
            };
        } catch (err) {
            return { sucess: false ,message: "Failed to update queue" };
        }
    } catch (err) {
        return { success: false, message: err.message };
    }
}



module.exports = {
    getQueueById,
    addQueue,
    updateQueue,
    getAll,
    getQueueByEventId,
    getEnabledQueueByEventId,
    deactivateQueueByEventId,
    deactivateQueueByUserId,
    getAllActiveEventQueue,
    deactivateQueueById,
    checkIfUserRaisedHand
}