const express = require('express');
const router = express.Router();
let {  getById,add,getAll,getByEventId,getByUserId } = require('../controllers/userEventController')
const auth = require("../middleware/auth");


/**
 * @swagger
 * /participant:
 *   post:
 *     parameters:
 *      - in: body
 *        name: userEvent
 *        description: New participant
 *        schema:
 *          type: object
 *          properties:
 *            event:
 *              type: string
 *            user:
 *              type: string
 *            displayName:
 *               type: string
 *     responses:
 *       201:
 *         description: Created
 */
router.post('/',async (req, res) => {
    console.log("under event");
    console.log(req.body);
    let body = {
        event: req.body.event,
        user: req.body.user,
        displayName: req.body.displayName
    };
    console.log(body);
    let response = await add(body);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(404).json(response);
    }
});



/**
 * @swagger
 * /participant:
 *   get:
 *     description: All participants
 *     responses:
 *       200:
 *         description: Returns all the participants
 */
 router.get('/', async (req, res) => {
    let response = await getAll();
    if (response.success == true) {
        res.status(200).json(response);
    } else {
        res.status(404).json(response);
    }
});

/**
 * @swagger
 * /participant/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The userevent ID.
 *     description: Get userevent by id
 *     responses:
 *       200:
 *         description: Returns the requested catachphrase
 */
 router.get('/:id', async (req, res) => {
    let response = await getById(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /participant/event/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get participant by event id
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/event/:id', async (req, res) => {
    let response = await getByEventId(req.params.id);
    res.json(response);
});


module.exports = router;