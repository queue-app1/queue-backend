const express = require('express');
const router = express.Router();
let { register, login, getAll} = require('../controllers/userController')
const auth = require("../middleware/auth");


/**
 * @swagger
 * /user:
 *   post:
 *     parameters:
 *      - in: body
 *        name: user
 *        description: New user
 *        schema:
 *          type: object
 *          properties:
 *            username:
 *              type: string
 *            password:
 *              type: string
 *     responses:
 *       201:
 *         description: Created
 */
router.post('/',async (req, res) => {
    console.log("under post");
    console.log(req.body);
    let body = {
        username: req.body.username,
        password: req.body.password,
    };
    console.log(body);
    let response = await register(body);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(404).json(response);
    }
});


/**
 * @swagger
 * /user/login:
 *   post:
 *     parameters:
 *      - in: body
 *        name: user
 *        description: New user
 *        schema:
 *          type: object
 *          properties:
 *            username:
 *              type: string
 *            password:
 *              type: string
 *     responses:
 *       201:
 *         description: Created
 */
 router.post('/login', async (req, res) => {
    let body = {
        username: req.body.username,
        password: req.body.password,
    };
    let response = await login(body);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(401).json(response);
    }
});

/**
 * @swagger
 * /user:
 *   get:
 *     description: All users
 *     responses:
 *       200:
 *         description: Returns all the users
 */
 router.get('/', async (req, res) => {
    let response = await getAll();
    if (response.success == true) {
        res.status(200).json(response);
    } else {
        res.status(404).json(response);
    }
});


module.exports = router;