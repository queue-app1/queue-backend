const express = require('express');
const router = express.Router();
let { addQueue, checkIfUserRaisedHand, getAllActiveEventQueue, deactivateQueueByEventId,deactivateQueueById,deactivateQueueByUserId, updateQueue, getAll, getQueueByEventId, getEnabledQueueByEventId, getQueueById } = require('../controllers/queueController')
const auth = require("../middleware/auth");


/**
 * @swagger
 * /queue:
 *   post:
 *     parameters:
 *      - in: body
 *        name: queue
 *        description: New queue
 *        schema:
 *          type: object
 *          properties:
 *            event:
 *              type: string
 *            user:
 *              type: string
 *            displayName:
 *               type: string
 *            active:
 *               type: boolean
 *     responses:
 *       201:
 *         description: Created
 */
router.post('/',async (req, res) => {
    console.log("under event");
    console.log(req.body);
    let body = {
        event: req.body.event,
        user: req.body.user,
        displayName: req.body.displayName,
        active: req.body.active
    };
    console.log(body);
    let response = await addQueue(body);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(404).json(response);
    }
});


/**
 * @swagger
 * /queue/{id}:
 *   patch:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The queue ID.
 *      - in: body
 *        name: event
 *        description: Update queue
 *        schema:
 *          type: object
 *          properties:
 *            event:
 *              type: string
 *            user:
 *              type: string
 *     responses:
 *       201:
 *         description: Created
 */
 router.put('/:id', async (req, res) => {
    let disabled = false;
    if (req.body.disabled) {disabled = req.body.disabled}
    let response = await updateQueue(req.params.id, disabled);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(404).json(response);
    }
});

/**
 * @swagger
 * /queue:
 *   get:
 *     description: All queues
 *     responses:
 *       200:
 *         description: Returns all the queues
 */
 router.get('/', async (req, res) => {
    let response = await getAll();
    if (response.success == true) {
        res.status(200).json(response);
    } else {
        res.status(404).json(response);
    }
});


/**
 * @swagger
 * /queue/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The queue ID.
 *     description: Get queue by id
 *     responses:
 *       200:
 *         description: Returns the requested catachphrase
 */
 router.get('/:id', async (req, res) => {
    let response = await getQueueById(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /queue/active/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The queue ID.
 *     description: Get queue by id
 *     responses:
 *       200:
 *         description: Returns the requested catachphrase
 */
 router.get('/active/:id', async (req, res) => {
    let response = await getAllActiveEventQueue(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /queue/event/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get queue by event id
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/event/:id', async (req, res) => {
    let response = await getQueueByEventId(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /queue/event/deactivate/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get queue by event id
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/event/deactivate/:id', async (req, res) => {
    let response = await deactivateQueueByEventId(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /queue/deactivate/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get queue by event id
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/deactivate/:id', async (req, res) => {
    let response = await deactivateQueueById(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /queue/user/deactivate/{userId}/{eventId}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: lower users raisen hand
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/user/deactivate/:userId/:eventId', async (req, res) => {
    let response = await deactivateQueueByUserId(req.params.eventId, req.params.userId);
    res.json(response);
});

/**
 * @swagger
 * /queue/user/raiseHand/{userId}/{eventId}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: userId
 *        required: true
 *        type: string
 *        description: The user ID.
 *      - in: path
 *        name: eventId
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: check if user raise his hand
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/user/raiseHand/:userId/:eventId', async (req, res) => {
     console.log("under check if user");
     console.log(req.params)
    let response = await checkIfUserRaisedHand(req.params.userId, req.params.eventId);
    res.json(response);
});

/**
 * @swagger
 * /queue/userEvent/{user}/{event}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get queue by event id
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/userEvent/:user/:event', async (req, res) => {
    let response = await checkIfUserRaisedHand(req.params.user, req.params.event);
    res.json(response);
});

/**
 * @swagger
 * /queue/event/active/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get queue by event id
 *     responses:
 *       200:
 *         description: Returns the requested events
 */
 router.get('/event/active/:id', async (req, res) => {
    let response = await getEnabledQueueByEventId(req.params.id);
    res.json(response);
});
module.exports = router;