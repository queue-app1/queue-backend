const catchphrases = require('./catchphrases')
const user = require('./user')
const event = require('./event')
const queue = require('./queue')
const participant = require('./userEvent')

module.exports = {
    catchphrases,
    user,
    event,
    queue,
    participant
}