const express = require('express');
const router = express.Router();
let { addEvent, updateEvent, getAll, getEventById, getEventSummaryById } = require('../controllers/eventController')
const auth = require("../middleware/auth");


/**
 * @swagger
 * /event:
 *   post:
 *     parameters:
 *      - in: body
 *        name: event
 *        description: New event
 *        schema:
 *          type: object
 *          properties:
 *            title:
 *              type: string
 *            startDate:
 *              type: string
 *              format: date
 *            endDate:
 *              type: string
 *              format: date-time
 *            has_ended:
 *              type: boolean
 *     responses:
 *       201:
 *         description: Created
 */
router.post('/',async (req, res) => {
    console.log("under event");
    console.log(req.body);
    let body = {
        title: req.body.title,
        startDate: req.body.startDate,
        has_ended: false
    };
    console.log(body);
    let response = await addEvent(body);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(404).json(response);
    }
});


/**
 * @swagger
 * /event/{id}:
 *   patch:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *      - in: body
 *        name: event
 *        description: Update event
 *        schema:
 *          type: object
 *          properties:
 *            title:
 *              type: string
 *            startDate:
 *              type: date
 *            endDate:
 *              type: date
 *            has_ended:
 *              type: boolean
 *     responses:
 *       201:
 *         description: Created
 */
 router.put('/:id', async (req, res) => {
    let title = null, startDate = null, endDate = null, has_ended = false;
    if (req.body.title) {title = req.body.title}
    if (req.body.startDate) {startDate = req.body.startDate}
    if (req.body.endDate) {endDate = req.body.endDate}
    if (req.body.has_ended) {has_ended = req.body.has_ended}
    let body ={
        title: req.body.title,
        startDate: req.body.startDate,
        has_ended: req.body.has_ended,
        queueOn: req.body.queueOn
    }
    let response = await updateEvent(req.params.id, body);

    if (response.success == true) {
        res.status(201).json(response);
    } else {
        res.status(404).json(response);
    }
});

/**
 * @swagger
 * /event:
 *   get:
 *     description: All events
 *     responses:
 *       200:
 *         description: Returns all the events
 */
 router.get('/', async (req, res) => {
    let response = await getAll();
    if (response.success == true) {
        res.status(200).json(response);
    } else {
        res.status(404).json(response);
    }
});

/**
 * @swagger
 * /event/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get an event by id
 *     responses:
 *       200:
 *         description: Returns the requested event
 */
 router.get('/:id', async (req, res) => {
    let response = await getEventById(req.params.id);
    res.json(response);
});

/**
 * @swagger
 * /event/summary/{id}:
 *   get:
 *     parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        type: string
 *        description: The event ID.
 *     description: Get an event by id
 *     responses:
 *       200:
 *         description: Returns the requested event
 */
 router.get('/summary/:id', async (req, res) => {
    let response = await getEventSummaryById(req.params.id);
    res.json(response);
});

module.exports = router;